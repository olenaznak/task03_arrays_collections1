package com.olenaznak;

import java.util.Scanner;

public class ConsoleInput {
    public static int getInt(String msg) {
        System.out.println(msg);
        Scanner in = new Scanner(System.in);
        while (!in.hasNextInt()) {
            System.out.println("Incorrect input, try one more time");
            in.next();
        }
        return in.nextInt();
    }

    public static int getInt() {
        Scanner in = new Scanner(System.in);
        while (!in.hasNextInt()) {
            System.out.println("Incorrect input, try one more time");
            in.next();
        }
        return in.nextInt();
    }

    public static int[] getIntArray(String msg) {
        System.out.println(msg);
        System.out.println("Input length");
        int length = getInt();
        int[] arr = new int[length];
        for (int i = 0; i < length; i++) {
            arr[i] = getInt();
        }
        return arr;
    }

    public static boolean getConfirmation() {
        while (true) {
            String confirm = getString();
            if (confirm.equals("yes")) {
                return true;
            }
            if (confirm.equals("no")) {
                return false;
            }
            System.out.println("Incorrect input, try one more time");
        }
    }

    public static String getString() {
        String str;
        Scanner in = new Scanner(System.in);
        while (!in.hasNextLine()) {
            System.out.println("Incorrect input, try one more time");
            in.next();
        }
        str = in.nextLine();
        return str;
    }
}
