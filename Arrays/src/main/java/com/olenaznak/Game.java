package com.olenaznak;

import java.util.Arrays;
import java.util.Random;

public class Game {
    private int INITIAL_POWER = 25;
    private int ROOMS_AMOUNT = 10;
    private int LOWER_ARTIFACT_POWER = 10;
    private int UPPER_ARTIFACT_POWER = 80;
    private int LOWER_MONSTER_POWER = 5;
    private int UPPER_MONSTER_POWER = 100;
    private int TYPES_AMOUNT = 2;

    private int heroPower = INITIAL_POWER;
    private Room[] rooms = new Room[ROOMS_AMOUNT];
  //  private Room[] rooms = {new Room(2, 52), new Room(2, 72), new Room(1, 49),
    //      new Room(2, 65), new Room(1, 57), new Room(1, 25), new Room(2, 11),
      //    new Room(2, 32), new Room(1, 74), new Room(1, 37)};


    private int getRandom(int b1, int b2) {
        Random rand = new Random();
        return rand.nextInt(b2 - b1 + 1) + b1;
    }

    public void fillRooms() {
        for (int i = 0; i < ROOMS_AMOUNT; i++) {
            rooms[i] = new Room(getRandom(1, 2));
            if (rooms[i].getType() == 1) {
                rooms[i].setPower(getRandom(LOWER_ARTIFACT_POWER, UPPER_ARTIFACT_POWER));
            } else {
                rooms[i].setPower(getRandom(LOWER_MONSTER_POWER, UPPER_MONSTER_POWER));
            }
        }
    }

    public void getRoomsInfo() {
        System.out.println("Rooms");
        for (int i = 0; i < ROOMS_AMOUNT; i++) {
            System.out.printf("%11d", i + 1);
        }
        System.out.println();
        for (int i = 0; i < ROOMS_AMOUNT; i++) {
            if (rooms[i].getType() == 1) {
                System.out.printf("%11s", "Artifact");
            } else {
                System.out.printf("%11s", "Monster");
            }
        }
        System.out.println();
        for (int i = 0; i < ROOMS_AMOUNT; i++) {
            System.out.printf("%11d", rooms[i].getPower());
        }
    }

    int getDeathRoomsAmount() {
        int count = 0;
        return counter(count, rooms, 0);
    }

    int counter(int n, Room[] rs, int st) {
        if (st == ROOMS_AMOUNT) {
            return n;
        }
        if (rs[st].getType() == 2 && rs[st].getPower() > heroPower) {
            n++;
        }
        return counter(n, rs, st + 1);
    }

    /*    int[] getSequenceOfRooms() {
            int[] nums = new int[ROOMS_AMOUNT];
            findSequence(nums, rooms, 0, 0);
            return nums;
        }*/
/*
    void findSequence(int[] nums, Room[] rs, int st, int ni){
        if(st == ROOMS_AMOUNT || isFill(nums))
            return;
        if(rs[st].getType() == 1){
            nums[ni++] = st+1;
            heroPower += rs[st].getPower();
        } else {
            if(rs[st].getPower() < heroPower){
                heroPower -= rs[st].getPower();
                nums[ni++] = st+1;
            }
        }
        findSequence(nums, rs, st+1, ni);
        findSequence(nums, rs, st-1, ni);
    }
*/
    private boolean isFill(int[] arr) {
        //Arrays.sort(arr);
        for (int i = 0; i < ROOMS_AMOUNT; i++) {
            if (arr[i] == 0) {
                return false;
            }
        }
        return true;
    }

    void findRoomsOrder() {
        int[] nums = new int[ROOMS_AMOUNT];
        int j = 0;
        int count = 0;
        while (!isFill(nums) && count < 10) {
            for (int i = 0; i < ROOMS_AMOUNT; i++) {
                if (isInArray(i + 1, nums)) {
                    continue;
                }
                if (rooms[i].getType() == 1) {
                    nums[j++] = i + 1;
                    heroPower += rooms[i].getPower();
                } else if (rooms[i].getPower() < heroPower) {
                    heroPower -= rooms[i].getPower();
                    nums[j++] = i + 1;
                }
            }
            count++;
        }
        if (count >= 10) {
            System.out.println("You can`t win");
        } else {
            ConsoleOutput.printArray(nums);
        }
    }

    boolean isInArray(int n, int[] nums) {
        for (int i = 0; i < ROOMS_AMOUNT; i++) {
            if (nums[i] == n) {
                return true;
            }
        }
        return false;
    }
}
