package com.olenaznak;

public class ConsoleOutput {
    public static void printArray(int[] arr) {
        for (int e : arr) {
            System.out.print(e + "  ");
        }
        System.out.println();
    }

    public static void printOptions() {
        System.out.println("Choose option!");
        System.out.println("Intersection-------------------------------1*");
        System.out.println("Difference---------------------------------2*");
        System.out.println("Delete more than two duplicates------------3*");
        System.out.println("Delete sequence of similar elements--------4*");
    }

    public static void printSubOptions(int c) {
        switch (c){
            case 1:
                System.out.println("With duplicates-----------1*");
                System.out.println("Without duplicates--------2*");
                break;
            case 2:
                System.out.println("With duplicates-----------1*");
                System.out.println("Without duplicates--------2*");
                break;
        }
    }
}
