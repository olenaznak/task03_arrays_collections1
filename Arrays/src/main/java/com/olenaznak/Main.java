package com.olenaznak;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //int[] arr1 = {1, 2, 3, 4, 5, 6, 9, 7, 13};
        //int[] arr2 = {2, 4, 6, 7, 8};
        //int[] arr3 = {1, 1, 1, 2, 3, 4, 4, 4, 5, 5, 5, 6, 6};
        //int[] arr4 = {1, 2, 1, 2, 1, 3, 4, 2, 3, 6};
       /* while (true) {
            ConsoleOutput.printOptions();
            int i = ConsoleInput.getInt();
            int si;
            int arr1[];
            int arr2[];
            boolean check = true;
            while (check) {
                switch (i) {
                    case 1:
                        findIntersection(i);
                        check = false;
                        break;
                    case 2:
                        findDifference(i);
                        check = false;
                        break;
                    case 3:
                        findArrayWithoutMoreThanTwoDuplicates();
                        check = false;
                        break;
                    case 4:
                        findArrayWithoutSequencesOfSimilarElements();
                        check = false;
                        break;
                    default:
                        System.out.println("Invalid option. Try one more time.");
                }
            }
            System.out.println("Would you like to continue? (yes/no)");
            if(!ConsoleInput.getConfirmation()){
                break;
            }
        }*/
       Game g = new Game();
       g.fillRooms();
       g.getRoomsInfo();
        System.out.println();
        System.out.println(g.getDeathRoomsAmount());
        g.findRoomsOrder();
    }

    private static void findIntersection(int i) {
        int[] arr1 = ConsoleInput.getIntArray("Input first array");
        int[] arr2 = ConsoleInput.getIntArray("Input second array");
        ConsoleOutput.printSubOptions(i);
        int si = ConsoleInput.getInt();
        boolean checkSI = true;
        while (checkSI) {
            switch (si) {
                case 1:
                    ConsoleOutput.printArray(ArrayProcessing.findArraysIntersection(
                            arr1, arr2, arr1.length, arr2.length));
                    checkSI = false;
                    break;
                case 2:
                    ConsoleOutput.printArray(ArrayProcessing.findArraysIntersectionDistinct(arr1, arr2));
                    checkSI = false;
                    break;
                default:
                    System.out.println("Invalid option. Try one more time.");
            }
        }
    }

    private static void findDifference(int i) {
        int[] arr1 = ConsoleInput.getIntArray("Input first array");
        int[] arr2 = ConsoleInput.getIntArray("Input second array");
        ConsoleOutput.printSubOptions(i);
        int si = ConsoleInput.getInt();
        boolean checkSI = true;
        while (checkSI) {
            switch (si) {
                case 1:
                    ConsoleOutput.printArray(
                            ArrayProcessing.findArraysDifference(arr1, arr2, arr1.length, arr2.length));
                    checkSI = false;
                    break;
                case 2:
                    ConsoleOutput.printArray(ArrayProcessing.findArraysDifferenceDistinct(arr1, arr2));
                    checkSI = false;
                    break;
                default:
                    System.out.println("Invalid option. Try one more time.");
            }
        }
    }

    private static void findArrayWithoutMoreThanTwoDuplicates() {
        int[] arr = ConsoleInput.getIntArray("Input first array");
        ConsoleOutput.printArray(
                ArrayProcessing.deleteMoreThanTwoElements(arr, arr.length));
    }

    private static void findArrayWithoutSequencesOfSimilarElements() {
        int[] arr = ConsoleInput.getIntArray("Input first array");
        ConsoleOutput.printArray(ArrayProcessing.deleteDuplicates(arr, arr.length));
    }
}
