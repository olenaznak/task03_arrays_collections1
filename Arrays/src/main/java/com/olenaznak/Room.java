package com.olenaznak;

public class Room {
    int type;
    int power;

    public Room(int type) {
        this.type = type;
    }

    public Room(int type, int power) {
        this.type = type;
        this.power = power;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
