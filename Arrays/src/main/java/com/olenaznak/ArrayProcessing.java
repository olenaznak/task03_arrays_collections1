package com.olenaznak;

import java.sql.Array;
import java.util.Arrays;

public class ArrayProcessing {
    final static int SIZE = 50;

    /**
     * Intersection of two arrays of unique elements using letgth and loops
     *
     * @param arr1    - fitst array
     * @param arr2    - srcond array
     * @param length1 - length of first array
     * @param length2 - length of second array
     * @return array of identical elements
     */

    public static int[] findArraysIntersection(int[] arr1, int[] arr2, int length1, int length2) {
        int length = length1 < length2 ? length1 : length2;
        int[] intersection = new int[length];
        int count = 0;
        int i = 0;
        int j = 0;
        Arrays.sort(arr1);
        Arrays.sort(arr2);
        while (i < length1 && j < length2) {
            if (arr1[i] < arr2[j]) {
                i++;
            } else if (arr1[i] > arr2[j]) {
                j++;
            } else {
                intersection[count++] = arr1[i];
                i++;
                j++;
            }
        }
        return resize(intersection, count);
    }

    /**
     * Intersection of two arrays of unique elements using streams
     *
     * @param arr1 - fitst array
     * @param arr2 - srcond array
     * @return array of identical elements
     */

    public static int[] findArraysIntersectionDistinct(int[] arr1, int[] arr2) {
        int[] intersection;
        intersection = Arrays.stream(arr1)
                .distinct()
                .filter(x -> Arrays.stream(arr2).anyMatch(y -> (y == x)))
                .toArray();

        return intersection;
    }

    /**
     * Difference of two arrays using letgth and loops
     *
     * @param arr1    - fitst array
     * @param arr2    - srcond array
     * @param length1 - length of first array
     * @param length2 - length of second array
     * @return array of unique elements of array 1
     */

    public static int[] findArraysDifference(int[] arr1, int[] arr2, int length1, int length2) {
        int length = length1 > length2 ? length1 : length2;
        int[] difference = new int[length];
        int count = 0;
        int i = 0;
        int j = 0;
        Arrays.sort(arr1);
        Arrays.sort(arr2);
        while (i < length1 && j < length2) {
            if (arr1[i] < arr2[j]) {
                difference[count++] = arr1[i];
                i++;
            } else if (arr1[i] > arr2[j]) {
                difference[count++] = arr1[i];
                j++;
            } else {
                i++;
                j++;
            }
        }
        while (i < length1){
            difference[count++] = arr1[i];
            i++;
        }
        return resize(difference, count);
    }

    /**
     * Difference of two arrays using streams
     *
     * @param arr1 - fitst array
     * @param arr2 - srcond array
     * @return array of identical elements
     */

    public static int[] findArraysDifferenceDistinct(int[] arr1, int[] arr2) {
        int[] difference;
        difference = Arrays.stream(arr1)
                .filter(x -> Arrays.stream(arr2).noneMatch(y -> (y == x)))
                .toArray();

        return difference;
    }

    /**
     * @param arr
     * @param length
     */

    public static int[] deleteMoreThanTwoElements(int[] arr, int length) {
        int[] arrCopy = new int[length];
        System.arraycopy(arr, 0, arrCopy, 0, length);
        ArrayElement[] checkArr = new ArrayElement[length];
        int count = 0;
        for (int i = 0; i < length; i++) {
            ArrayElement curEl = findElement(checkArr, length, arrCopy[i]);
            if (curEl != null) {
                if (curEl.getAmount() < 2) {
                    curEl.setAmount(curEl.getValue() + 1);
                } else {
                    delete(arrCopy, length, i);
                    length--;
                }
            } else {
                checkArr[count++] = new ArrayElement(arrCopy[i], 1);
            }
        }
        return resize(arrCopy, length);
    }

    /**
     * @param arrEl
     * @param el
     * @return
     */

    private static ArrayElement findElement(ArrayElement[] arrEl, int length, int el) {
        for (int i = 0; i < length; i++) {
            if (arrEl[i] == null) {
                return null;
            }
            if (arrEl[i].getValue() == el) {
               return arrEl[i];
            }
        }
        return null;
    }

    private static void delete(int[] arr, int length, int i) {
        for (int j = i; j < length - 1; j++) {
            arr[j] = arr[j + 1];
        }
    }

    public static int[] deleteDuplicates(int[] arr, int length) {
        int[] arrCopy = new int[length];
        System.arraycopy(arr, 0, arrCopy, 0, length);
        for (int i = 0; i < length - 1; i++) {
            int j = i + 1;
            while (arrCopy[i] == arrCopy[j] && j < length) {
                delete(arrCopy, length, arrCopy[j]);
                length--;
            }
        }
        return resize(arrCopy, length);
    }

    private static int[] resize(int[] arr, int length) {
        int[] newArr = new int[length];
        System.arraycopy(arr, 0, newArr, 0, length);
        return newArr;
    }
}
