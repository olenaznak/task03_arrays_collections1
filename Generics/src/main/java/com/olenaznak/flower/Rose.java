package com.olenaznak.flower;

public class Rose extends Flower {
    public Rose(String name, double price, String color, boolean isSmell, boolean isThorns) {
        super(name, price, color, isSmell, isThorns);
    }

    public Rose() {
    }

}
