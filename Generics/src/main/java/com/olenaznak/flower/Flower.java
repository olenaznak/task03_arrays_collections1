package com.olenaznak.flower;

import java.util.Objects;

public abstract class Flower {
    private String name;
    private double price;
    private String color;
    private boolean isSmell;
    private boolean isThorns;

    public Flower(String name, double price, String color, boolean isSmell, boolean isThorns) {
        this.name = name;
        this.price = price;
        this.color = color;
        this.isSmell = isSmell;
        this.isThorns = isThorns;
    }

    public Flower() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isSmell() {
        return isSmell;
    }

    public void setSmell(boolean smell) {
        isSmell = smell;
    }

    public boolean isThorns() {
        return isThorns;
    }

    public void setThorns(boolean thorns) {
        isThorns = thorns;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Flower)) return false;
        Flower flower = (Flower) o;
        return Double.compare(flower.price, price) == 0 &&
                isSmell == flower.isSmell &&
                isThorns == flower.isThorns &&
                Objects.equals(name, flower.name) &&
                Objects.equals(color, flower.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, color, isSmell, isThorns);
    }
}
