package com.olenaznak.flower;

public class Gerbera extends Flower {
    public Gerbera(String name, double price, String color, boolean isSmell, boolean isThorns) {
        super(name, price, color, isSmell, isThorns);
    }

    public Gerbera() {
    }
}

