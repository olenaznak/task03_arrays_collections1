package com.olenaznak.flower;

public class Lily extends Flower {
    public Lily(String name, double price, String color, boolean isSmell, boolean isThorns) {
        super(name, price, color, isSmell, isThorns);
    }

    public Lily() {
    }
}
