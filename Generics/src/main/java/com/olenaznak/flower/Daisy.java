package com.olenaznak.flower;

public class Daisy extends Flower {
    public Daisy(String name, double price, String color, boolean isSmell, boolean isThorns) {
        super(name, price, color, isSmell, isThorns);
    }

    public Daisy() {
    }
}
