package com.olenaznak.generics;

import java.util.ArrayList;
import java.util.List;

public class PriorityQueue<T extends Comparable> {
    private List<T> queue;

    public PriorityQueue() {
        queue = new ArrayList<T>();
    }

    public void insert(T item) {
        int i;
        if (queue.size() == 0) {
            queue.add(item);
        } else {
            for (i = queue.size() - 1; i >= 0; i--) {
                if (item.compareTo(queue.get(i)) <= 0) {
                    break;
                }
            }
            queue.add(i + 1, item);
        }
    }

    public T remove() {
        T firstItem = queue.get(queue.size() - 1);
        queue.remove(queue.size() - 1);
        return firstItem;
    }

    public T peek() {
        return queue.get(queue.size() - 1);
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }
}
