package com.olenaznak.generics;

import com.olenaznak.flower.Flower;

import java.util.ArrayList;
import java.util.List;

public class FlowerContainer<T extends Flower> {
    private List<T> list = new ArrayList<>();

    public void add(T f) {
        list.add(f);
    }

    public T get(int i) {
        return list.get(i);
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public int getSize() {
        return list.size();
    }

    public List<? extends T> get(List<? extends T> order) {
        List<T> bouquet = new ArrayList<>();
        for (T el : order) {
            if (list.contains(el)) {
                bouquet.add(el);
            }
        }
        return bouquet;
    }
}
