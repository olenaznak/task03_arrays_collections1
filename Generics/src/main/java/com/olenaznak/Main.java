package com.olenaznak;

import com.olenaznak.flower.*;
import com.olenaznak.flower.Flower;
import com.olenaznak.generics.FlowerContainer;
import com.olenaznak.generics.PriorityQueue;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main (String[] args) {
        FlowerContainer<Flower> fc = new FlowerContainer<Flower>();

        PriorityQueue<Integer> pq = new PriorityQueue<>();
        pq.insert(5);
        pq.insert(8);
        pq.insert(1);
        pq.insert(4);
        pq.insert(3);
        pq.insert(2);
        pq.insert(11);
        pq.insert(9);
        while (!pq.isEmpty()) {
            System.out.println(pq.remove());
        }
        System.out.println();
        System.out.println();

        fc.add(new Daisy());
        fc.add(new Rose());
        fc.add(new Gerbera());
        fc.add(new Gerbera());
        fc.add(new Lily());

        List<Flower> list = new ArrayList<>();
        list.add(new Daisy());
        list.add(new Gerbera());

        System.out.println(fc.get(list));
    }
}
