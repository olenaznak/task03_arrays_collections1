package com.olenaznak;

import java.util.ArrayList;
import java.util.List;

public class Deque<T> {
    List<T> list;

    public Deque() {
        list = new ArrayList<T>();
    }

    public void addFirst(T obj) {
        list.add(obj);
    }

    public void addLast(T obj) {
        list.add(list.size() - 1, obj);
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public int getSize() {
        return list.size();
    }
}
