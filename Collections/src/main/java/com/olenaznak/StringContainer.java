package com.olenaznak;

import java.util.Arrays;

public class StringContainer {
    private final int SIZE = 50;

    private String[] arr;
    private int size;

    public StringContainer() {
        arr = new String[SIZE];
    }

    public void add(String str) {
        //size++;
        if(size != 0 && size % SIZE == 0) {
            resize(size/SIZE + 1);
        }
        arr[size++] = str;
    }

    public String get(int i) {
        if (i < size) {
            return null;
        }
        return arr[i];
    }

    public String get(String str) {
        for(int i = 0; i < size; i++) {
            if(arr[i].equals(str)) {
                return str;
            }
        }
        return null;
    }

    public String[] getAll() {
        return arr;
    }

    public int getSize() {
        return size;
    }

    private void resize(int k) {
        String[] newArr = new String[k * SIZE];
        System.arraycopy(arr, 0, newArr, 0, size);
        arr = newArr;
    }
}
