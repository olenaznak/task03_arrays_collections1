package com.olenaznak;

public class TwoStringsContainer implements Comparable<String> {
    private String s1;
    private String s2;

    public TwoStringsContainer(String s1, String s2) {
        this.s1 = s1;
        this.s2 = s2;
    }

    public String getS1() {
        return s1;
    }

    public void setS1(String s1) {
        this.s1 = s1;
    }

    public String getS2() {
        return s2;
    }

    public void setS2(String s2) {
        this.s2 = s2;
    }

    @Override
    public int compareTo(String str) {
        return s1.compareTo(str);
    }
}
