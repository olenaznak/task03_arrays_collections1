package com.olenaznak;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        final int N = 1000000;
        List<String> list = new ArrayList<>();
        StringContainer container = new StringContainer();
        long start;
        long time;
        start = new Date().getTime();
        for (int i = 0; i < N; i++) {
            list.add("str" + i);
        }
        System.out.println("Test ArrayList vs StringContainer");
        time = new Date().getTime() - start;
        System.out.println("Adding time (ArrayList) = " + time);

        start = new Date().getTime();
        for (int i = 0; i < N; i++) {
            container.add("str" + i);
        }
        time = new Date().getTime() - start;
        System.out.println("Adding time (StringContainer) = " + time);

        start = new Date().getTime();
        for (int i = 0; i < N; i++) {
            list.get(i);
        }
        time = new Date().getTime() - start;
        System.out.println("Getting time (ArrayList) = " + time);

        start = new Date().getTime();
        for (int i = 0; i < N; i++) {
            container.get(i);
        }
        time = new Date().getTime() - start;
        System.out.println("Getting time (StringContainer) = " + time);
    }
}
