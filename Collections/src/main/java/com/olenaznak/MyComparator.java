package com.olenaznak;

import java.util.Comparator;

public class MyComparator implements Comparator<TwoStringsContainer> {

    @Override
    public int compare(TwoStringsContainer o1, TwoStringsContainer o2) {
        return o1.getS2().compareTo(o2.getS2());
    }
}
